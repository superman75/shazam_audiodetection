import threading
import ttk
from Tkinter import *
from dejavu import Dejavu
from dejavu.recognize import FileRecognizer
import webbrowser
import time
import datetime
from ffmpy import FFmpeg
import json
import os
import MySQLdb
import subprocess
import thread
import csv
import commands

# if db is loaded already
db_loaded = False
matched_song = []
siteUrls = dict()
url = ""

# Loading config from a JSON file (or anything outputting a python dictionary)
with open("res/dbfile") as f:
    config = json.load(f)


with open("res/sitelist.txt") as f:
    for line in f:
        siteUrls[line.split("\t")[0].strip()] = [line.split("\t")[1].strip(), line.split("\t")[2].strip()]



# global variables
djv = Dejavu(config)
cur_dir = os.getcwd() + '\\res\\mp3'
music_list = []
onRec = False

# Recognize music using fingerprint
def RecognizeMusin():
    song = djv.recognize(FileRecognizer, 'res/temp.mp3')
    return song


# find matched full file name
def find(name, path):
    for root, dirs, files in os.walk(path):
        for file in files:
            if name in file:
                return file

db = MySQLdb.connect(host = "127.0.0.1",  # your host, usually localhost
                         user = "root",  # your username
                         passwd = "diwifk",  # your password
                         db = "dejavu")  # name of the data base

# initializing gui content (add last time column)
def guiInit():
    cur = db.cursor()
    try:
        cmd = 'ALTER TABLE songs ADD COLUMN last_time VARCHAR(50) after file_sha1, ADD COLUMN Singer VARCHAR(50) after last_time, ADD COLUMN Gender VARCHAR(50) after Singer, ADD COLUMN Artist VARCHAR(50) after Gender, ADD COLUMN client_name VARCHAR(50) after Artist;'
        cur.execute(cmd)
        cur.commit()
    except:
        pass

    try:
        cmd = 'ALTER TABLE songs ADD COLUMN client_name VARCHAR(50) after Artist;'
        cur.execute(cmd)
        cur.commit()
    except:
        pass

    global db_loaded
    if db_loaded == True:
        return 0

    # Building database
    djv = Dejavu(config)
    djv.fingerprint_directory("res/mp3", [".mp3", ".wma", ".wav"])

    cur = db.cursor()
    cmd = 'SELECT song_name, Singer, Gender, Artist, last_time FROM songs;'
    cur.execute(cmd)
    rsts = cur.fetchall()
    # print rsts

    for rst in rsts:
        # print find(rst[0], cur_dir) + '\t\t' + str(rst[1])
        music_list.append(find(rst[0], cur_dir) + ' (' + str(rst[1]) + ')')

    music_list.sort()

    # listBox
    for music in music_list:
        listMusic.insert(END, music)

    db_loaded = True


# Gui main class
class Application(Frame):
    def __init__(self, master = None):
        Frame.__init__(self, master)
        self.pack()
        self.createWidget()
        self.master.protocol("WM_DELETE_WINDOW", self.quitWidget)
        guiInit()

    def quitWidget(self):
        print "Destroy Tkinter..."
        db.close()
        thread.exit_thread()
        self.destroy()
        self.quit()

    def createWidget(self):
        print "Create Widget..."


####################################################
### Building gui component
####################################################
root = Tk()
root.geometry("300x500+450+150")
root.resizable(width = False, height = False)
root.title("Music Recognition System (Demo)")
root.config(background = "seashell3")


####################################################
### Variables and functions
####################################################
# Radio station URL address
url_input = StringVar()
# match status text
match_show_text = StringVar()
match_show_text.set("There is no matched file.")
box_value = StringVar()
# radio / file flip
var1 = IntVar()

def onSearch():
    # if len(threading.enumerate()) > 1:
    #     print type(threading.enumerate)
    #     thread.exit_thread()
    # else:
    #     print "11111111111111111111111111111111111111111111"

    if var1.get() == 1:
        print "Search button is clicked..."
        global url
        url = url_input.get()
        if url is not None:
            webbrowser.open(url)

        global onRec
        if not onRec:
            onRec = True
            th = thread.start_new_thread(onCompare, ())

    else:
        global url
        url = siteUrls[box_value.get()][1]

        if url is not None:
            webbrowser.open(siteUrls[box_value.get()][0])

        global onRec
        if not onRec:
            onRec = True
            th = thread.start_new_thread(onCompare, ())


def onCompare():
    # global thread_check
    while True:
        time.sleep(5)
        
        print "Compare button is clicked..."
        global url
        site_url = url

        if site_url == 'none':
            continue

        rec_ok = False

        out_file = 'res/temp.mp3'
        cmd = "ffmpeg -t 5 -i \"" + site_url + "\" -y " + out_file

        print cmd

        # process = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        # stdout, stderr = process.communicate()
        #
        # print '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
        # if (len(stderr) != 0):
        #     print stderr
        #
        # print '--------------------------------------------------------'
        # if (len(stdout) != 0):
        #     print stdout
        #
        # print '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
        # rec_ok = True

        try:
            output = subprocess.check_output(cmd, stderr = subprocess.STDOUT, shell = True, universal_newlines = True)
        except subprocess.CalledProcessError as exc:
            # print("Status : FAIL", exc.returncode, exc.output)
            rec_ok = False
        else:
            # print("Output: \n{}\n".format(output))
            rec_ok = True

        print rec_ok

        # print '>>> ', rec_ok
        # compare
        if rec_ok == True:
            song = djv.recognize(FileRecognizer, out_file)
            print song

            if song is None:
            	continue

            if song['confidence'] > 50:
                print "Successfully matched..."
                print find(song['song_name'], cur_dir)
                s_name = find(song['song_name'], cur_dir)
                match_show_text.set(s_name)
                for i in range(len(music_list)):
                    if s_name in music_list[i]:
                        listMusic.delete(i)

                        now = datetime.datetime.now()
                        now_date = now.strftime("%d/%m/%Y")
                        now_time = now.strftime("%H:%M:%S")

                        # print now_time

                        db_name = music_list[i].split(' (')[0]
                        listMusic.insert(i, db_name + ' (' + now_time + ')')

                        if db_name in matched_song:
                            pass
                        else:
                            cur = db.cursor()
                            matched_song.append(db_name)

                            # print db_name.split('.')[0]

                            cmd = 'SELECT client_name FROM songs WHERE song_name = \'' + db_name.split('.')[0] + '\';'
                            cur.execute(cmd)
                            song_id = cur.fetchall()

                            # print "======================================================"
                            # print str(song_id)

                            if len(song_id) == 0:
                                cmd = 'INSERT INTO log_table (song_name, start_time, client_name, start_date) VALUES (\'' + db_name + '\', \'' + str(now_time) + '\', \'' + "None" + '\', \'' + str(now_date) + '\')'
                            else:
                                cmd = 'INSERT INTO log_table (song_name, start_time, client_name, start_date) VALUES (\'' + db_name + '\', \'' + str(now_time) + '\', \'' + str(song_id[0][0]) + '\', \'' + str(now_date) + '\')'


                            # print cmd
                            cur.execute(cmd)
                            db.commit()

                        break
            else:
                # print "No matched music..."
                match_show_text.set("No matched music...")
                if len(matched_song):
                    db_name = matched_song[0]
                    now_time = time.asctime(time.localtime(time.time()))

                    now = datetime.datetime.now()
                    now_time1 = now.strftime("%H:%M:%S")

                    cur = db.cursor()
                    cmd = 'UPDATE log_table SET end_time = \'%s\' WHERE  song_name = \'%s\'' % (str(now_time1), db_name)
                    # cmd = 'INSERT INTO log_table (song_name, end_time) VALUES (' + db_name + ', ' + str(now_time) + ')'
                    cur.execute(cmd)
                    db.commit()
                    cmd = 'UPDATE songs SET last_time = \'%s\' WHERE  song_name = \'%s\'' % (now_time, db_name.split('.')[0])
                    cur.execute(cmd)
                    db.commit()

                    for i in range(len(music_list)):
                        if db_name in music_list[i]:
                            listMusic.delete(i)
                            listMusic.insert(i, db_name + ' (' + now_time + ')')
                    del matched_song[:]

            print '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', 'END OF MATCH'


def onRemoveMusic():
    print 'Remove music button is clicked...'
    selection = listMusic.curselection()
    list_val = listMusic.get(selection)

    listMusic.delete(selection)
    cur = db.cursor()
    try:
        song_full_name = list_val.split(' (')[0]
        # delete file
        os.remove(cur_dir + '/' + song_full_name)

        song_db_name = song_full_name.split('.')[0]
        cmd = 'SELECT song_id FROM songs WHERE song_name = \'' + str(song_db_name) + '\';'
        cur.execute(cmd)
        song_id = cur.fetchall()

        cmd = 'DELETE FROM fingerprints WHERE song_id = ' + str(song_id[0][0]) + ';'
        cur.execute(cmd)
        db.commit()
        cmd = 'DELETE FROM songs WHERE song_name = \'' + str(song_db_name) + '\';'
        cur.execute(cmd)
        db.commit()
    except:
        pass

def onPublishMusic():
    print 'Update music button is clicked...'
    cur = db.cursor()
    cmd = 'SELECT * FROM log_table;'
    cur.execute(cmd)
    rst = cur.fetchall()
    csv_rst = list()
    csv_rst.append(['No', 'Song Name', 'Client Name', 'Start date', 'Start Time', 'End Time'])
    for i in range(len(rst)):
        csv_rst.append([str(i + 1), rst[i][1], rst[i][2], rst[i][3], rst[i][4], rst[i][5]])
    with open('report.csv', 'wb') as myfile:
        wr = csv.writer(myfile, quoting = csv.QUOTE_ALL)
        for i in csv_rst:
            wr.writerow(i)

def OnFlip():
    print var1.get()
    checked = var1.get()
    if checked == 0:
        eUrl.grid_forget()
        coltbox.grid(row = 0, column = 1)
    else:
        coltbox.grid_forget()
        eUrl.grid(row = 0, column = 1)
####################################################
### Declaring Frames
####################################################
TopFrame = Frame(root, background = 'seashell3')
TopFrame.pack()
UrlFrame = Frame(root, background = 'seashell3')
UrlFrame.pack(fill = X)
MusicFrame = LabelFrame(root, font = ("arial", 10, "normal"), text = 'Adverts',
                        background = 'seashell3')
MusicFrame.pack(fill = 'both', expand = 'yes', padx = 10, pady = 10)


####################################################
### Component Layout
####################################################
# TopFrame
lblTitle = Label(TopFrame, font = ("arial", 12, "bold"), text = "Audiunt Recognition System",
                 fg = "midnight blue", background = 'seashell3', bd = 10, anchor = "w")
lblTitle.grid(row = 0, column = 0)

# UrlFrame
upperFrame = Frame(UrlFrame, background = 'seashell3')
upperFrame.pack(fill = X)
lowerFrame = Frame(UrlFrame, background = 'seashell3')
lowerFrame.pack(fill = X, padx = 12)
lblUrl = Label(upperFrame, font = ("arial", 10, "bold"), text = "Audio File", fg = 'brown3',
               background = 'seashell3', bd = 10, anchor = "w")
lblUrl.grid(row = 0, column = 0)
eUrl = Entry(upperFrame, font = ("arial", 9, "bold"), width = 22, textvariable = url_input, bd = 5,
             bg = "powder blue")
# eUrl.grid(row = 0, column = 1)

coltbox = ttk.Combobox(upperFrame, font = ("arial", 10, "bold"), width = 20, textvariable=box_value, state='readonly')
coltbox['values'] = sorted(siteUrls.keys())
coltbox.current(0)
coltbox.grid(row = 0, column = 1)

chkbox = Checkbutton(upperFrame, text="R/F", variable=var1, background = 'seashell3', command=OnFlip)
chkbox.grid(row = 0, column = 2)

btnOpen = Button(lowerFrame, fg = "navy", width = 12, font = ('arial', 9, 'bold'), text = "Search",
                 bg = "SkyBlue1", command = lambda: onSearch())
btnOpen.pack(side = LEFT, fill = X, expand = YES, padx = 20)
# btnClose = Button(lowerFrame, fg = "navy", width = 12, font = ('arial', 9, 'bold'), text = "Compare",
#                   bg = "SkyBlue1")
# btnClose.pack(side = LEFT, fill = X, expand = YES, padx = 5)

# Music information
lblMatchShow = Label(MusicFrame, width = 25, font = ("arial", 12, "bold"), bg = 'alice blue', textvariable = match_show_text)
lblMatchShow.pack(fill = Y, pady = 4)
listMusic = Listbox(MusicFrame, font = ("arial", 10), bd = 5)
listMusic.pack(fill = BOTH, expand = YES, padx = 4)
sbl = Scrollbar(MusicFrame, orient = HORIZONTAL)
sbl.pack(fill = X, padx = 4)

listMusic.configure(xscrollcommand = sbl.set)
sbl.configure(command = listMusic.xview)

# btnAddMusic = Button(MusicFrame, fg = "navy", font = ('arial', 8, 'bold'), width = 5, text = "Add",
#                      bg = "Slategray2", command = lambda: onAddMusic())
# btnAddMusic.pack(side = LEFT, fill = X, expand = YES, pady = 3, padx = 1)
btnRemoveMusic = Button(MusicFrame, fg = "navy", font = ('arial', 8, 'bold'), width = 5, text = "Remove",
                        bg = "Slategray2", command = lambda: onRemoveMusic())
btnRemoveMusic.pack(side = LEFT, fill = X, expand = YES, pady = 3, padx = 1)
btnPublishMusic = Button(MusicFrame, fg = "navy", font = ('arial', 8, 'bold'), width = 5, text = "Publish",
                        bg = "Slategray2", command = lambda: onPublishMusic())
btnPublishMusic.pack(side = LEFT, fill = X, expand = YES, pady = 3, padx = 1)


if __name__ == '__main__':

    cur = db.cursor()
    cmd = '''
        CREATE TABLE IF NOT EXISTS log_table (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        song_name VARCHAR(30) NOT NULL,
        client_name VARCHAR(30) DEFAULT NULL,
        start_date VARCHAR(30) DEFAULT NULL,
        start_time VARCHAR(30) DEFAULT NULL,
        end_time VARCHAR(30) DEFAULT NULL
        ) 
    '''
    cur.execute(cmd)

    app = Application(master = root)
    app.mainloop()

