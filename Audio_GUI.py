from Tkinter import *
import Tkinter as tk
import ttk

import datetime
import random, string
class SimpleTable(tk.Frame):
    def __init__(self, parent, rows=10, columns=2):
        # use black background so it "peeks through" to
        # form grid lines
        tk.Frame.__init__(self, parent, background="black")
        self._widgets = []
        for row in range(rows):
            current_row = []
            for column in range(columns):
                label = tk.Label(self, text="%s/%s" % (row, column),
                                 borderwidth=0, width=10)
                label.grid(row=row, column=column, sticky="nsew", padx=1, pady=1)
                current_row.append(label)
            self._widgets.append(current_row)

        for column in range(columns):
            self.grid_columnconfigure(column, weight=1)


    def set(self, row, column, value):
        widget = self._widgets[row][column]
        widget.configure(text=value)
def createRandomStrings(l, n):
    """create list of l random strings, each of length n"""
    names = []
    for i in range(l):
        val = ''.join(random.choice(string.ascii_lowercase) for x in range(n))
        names.append(val)
    return names

def createData(rows=20, cols=5):
    """Creare random dict for test data"""

    data = {}
    names = createRandomStrings(rows,16)
    colnames = createRandomStrings(cols,5)
    for n in names:
        data[n]={}
        data[n]['label'] = n
    for c in range(0,cols):
        colname=colnames[c]
        vals = [round(random.normalvariate(100,50),2) for i in range(0,len(names))]
        vals = sorted(vals)
        i=0
        for n in names:
            data[n][colname] = vals[i]
            i+=1
    return data
class Application(Frame):
    def say_hi(self):
        print "hi there, everyone!"

    def createWidgets(self):
        print "OK!"
        self.urlLIst = tk

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack()
        self.createWidgets()

root = Tk()
root.geometry("600x500+0+0")
root.resizable(width=False,height=False)
root.title("Music Recognition System")

url_Input = StringVar()
songname_Input = StringVar()
singer_Input = StringVar()
artist_Input = StringVar()
matching_result = StringVar()
database_Input = StringVar()
url_list = StringVar()

Tops = Frame(root, width = "600",height = "50", relief = SUNKEN)
Tops.pack(side = TOP)

f4 = Frame(root, width = "300", height = "400", relief = SUNKEN)
f4.pack(side = RIGHT)

f1 = Frame(root, width = "300",height = "50", relief = SUNKEN)
f1.pack()

f3 = Frame(root, width = "200",height = "50", relief = SUNKEN)
f3.pack()

f2 = Frame(root, width = "200",height = "100",  relief = SUNKEN)
f2.pack()


#==================================Time=================================

# localtime = time.asctime(time.localtime(time.time()))
localtime = datetime.date.today().strftime("%Y-%m-%d")

#==================================Info==================================

lblInfo = Label(Tops, font=("arial",15,"bold"), text = "Music Recognition System", fg = "Steel Blue", bd = 10, anchor = "w")
lblInfo.grid(row = 0, column = 0)

lblInfo = Label(Tops, font=("arial",10,"bold"), text = localtime , fg = "Steel Blue", bd = 10, anchor = "w")
lblInfo.grid(row = 1, column = 0)

#============================= music site address =========================

urlInfo = Label(f1, font=("arial",10,"bold"), text = "Radio Station" , fg = "Steel Blue", bd = 10, anchor = "w")
urlInfo.grid(row = 0, column = 0)
urlAddress = Entry(f1, font=("arial",10,"bold"), textvariable = url_Input , bd = 5, insertwidth = 2, bg = "powder blue")
urlAddress.grid(row = 0, column = 1)

#=============================== Match Result ================================

matchResult = Label(f3, font = ("arial", 10), text = "Please display the matching result here", fg = "Steel Blue", bd = 10, anchor = "w")
matchResult.grid(row = 0, column = 0)

#============================= buttons =====================================
def listen_click():
     print "ListenBtn is clicked!"


def record_click():
    print "RecordBtn is clicked!"


ListenBtn = Button(f1, padx = 10, pady = 2, fg = "black", font=('arial',10,'bold'),
                   text ="Listen",bg = "powder blue", command=lambda: listen_click()).grid(row=1,column=0)
RecordBtn = Button(f1, padx = 10, pady = 2, fg = "red", font=('arial',10,'bold'),
                   text ="Record",bg = "powder blue", command=lambda: record_click()).grid(row=1,column=1)

# ================================== songs data ==================================
def Input_click():
    print "InputBtn is clicked!"


songInfo = Label(f2, font = ("arial", 10, "bold"), text = "Music Data", fg = "Steel Blue", bd = 10, anchor = "w")
songInfo.grid(columnspan = 2)

songNameInfo = Label(f2, font = ("arial", 10, "bold"), text = "song name", fg = "Steel Blue", bd = 10, anchor = "w")
songNameInfo.grid(row = 1, column = 0)
songName = Entry(f2, font = ("arial", 10), textvariable = songname_Input, bd = 5, insertwidth = 2, bg = "powder blue")
songName.grid(row = 1, column = 1)


singerInfo = Label(f2, font = ("arial", 10, "bold"), text = "singer", fg = "Steel Blue", bd = 10, anchor = "w")
singerInfo.grid(row = 2, column = 0)
singerName = Entry(f2, font = ("arial", 10), textvariable = singer_Input, bd = 5, insertwidth = 2, bg = "powder blue")
singerName.grid(row = 2, column = 1)

genderInfo = Label(f2, font = ("arial", 10, "bold"), text = "gender", fg = "Steel Blue", bd = 10, anchor = "w")
genderInfo.grid(row = 3, column = 0)
maleButton = Radiobutton(f2, text = 'M', value = 1)
maleButton.grid(row=3, column = 1)
femaleButton = Radiobutton(f2, text = 'W', value = 2)
femaleButton.grid(row=4, column = 1)

artistInfo = Label(f2, font = ("arial", 10, "bold"), text = "artist", fg = "Steel Blue", bd = 10, anchor = "w")
artistInfo.grid(row = 5, column = 0)
artistName = Entry(f2, font = ("arial", 10), textvariable = artist_Input, bd = 5, insertwidth = 2, bg = "powder blue")
artistName.grid(row = 5, column = 1)

InputBtn = Button(f2, padx = 10, pady = 2, fg = "black", font = ('arial', 10, 'bold'),
                  text = "Please input new song", bg = "powder blue", command = lambda: Input_click()).grid(
    columnspan = 2)
# ============================= display database songs =============================================

urlCombo = ttk.Combobox(f4,width = 5);
urlCombo.grid(row = 0,column = 0)
# urlList['values'] = ('A','B','C')
# urlList.grid(row = 0,column = 0, sticky = W)

# model = TableModel()
# databaseSong = createData(100,1)
# model.importDict(databaseSong)

# model.addColumn('comment')
# for i in model.reclist:
#     val = random.sample(['a', 'b', 'c'], 1)[0]
#     model.data[i]['comment'] = val
# # searchterms = [('label', 'aa', 'contains', 'AND'),
# #               ('label', 'bb', 'contains', 'OR')]
# searchterms = [('comment', 'a', '!=', 'AND'),
#                ('comment', 'b', '!=', 'AND')]
# table = TableCanvas(f4, model, width=250,height=200)
# table.configure(state = '')
# table.createTableFrame()
# databaseInfo.grid(row = 0, column = 0, columnspan = 6)
# ======= main ====================
app = Application(master = root)
app.mainloop()

