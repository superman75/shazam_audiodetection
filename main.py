import warnings
import json
from dejavu import Dejavu
from dejavu.recognize import FileRecognizer, MicrophoneRecognizer

# load config from a JSON file (or anything outputting a python dictionary)
with open("dbfile") as f:
    config = json.load(f)

def main():
    print "START >>> "

    isFingerprint = ""

    djv = Dejavu(config)

    # Fingerprint mp3 files

    while isFingerprint != "y" and isFingerprint != "n":
        isFingerprint = raw_input("Fingerprint is Finished (Yes: y, No: n): ")

    if isFingerprint == "n":
        djv.fingerprint_directory("mp3", [".mp3"])

    file_name = raw_input("Input File to recognize: ")
    song = djv.recognize(FileRecognizer, "mp3/" + file_name)
    print song

    print "END OF PROGRAM >>> "


if __name__ == '__main__':
    main()